
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Laravel social-CMS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <link rel="stylesheet" type="text/css" href="{{ asset("social/css/animate.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("social/css/bootstrap.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("social/css/line-awesome.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("social/css/line-awesome-font-awesome.min.css") }}">
    <link href="{{ asset("social/vendor/fontawesome-free/css/all.min.css") }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{ asset("social/css/font-awesome.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("social/css/jquery.mCustomScrollbar.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("social/lib/slick/slick.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("social/lib/slick/slick-theme.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("social/css/style.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("social/css/responsive.css") }}">
</head>

{{--<body oncontextmenu="return false;">--}}
<body>

<div class="wrapper">
    <header>
        <div class="container">
            <div class="header-data">
                <div class="logo">
                    <a href="#" title=""><img src="{{ asset("social/images/logo.png") }}" alt=""></a>
                </div><!--logo end-->
                <div class="search-bar">
                    <form>
                        <input type="text" name="search" placeholder="Search...">
                        <button type="submit"><i class="la la-search"></i></button>
                    </form>
                </div><!--search-bar end-->
                <nav>
                    <ul>
                        <li>
                            <a href="{{url('home')}}" title="">
                                <span><img src="{{ asset("social/images/icon1.png") }}" alt=""></span>
                                Home
                            </a>
                        </li>
                        <li>
                            <a href="profiles.html" title="">
                                <span><img src="{{ asset("social/images/icon4.png") }}" alt=""></span>
                                Profiles
                            </a>
                            <ul>
                                <li><a href="{{ url('userlist') }}" title="">User List</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="companies.html" title="">
                                <span><img src="{{ asset("social/images/icon2.png") }}" alt=""></span>
                                Companies
                            </a>
                            <ul>
                                <li><a href="companies.html" title="">Companies</a></li>
                                <li><a href="company-profile.html" title="">Company Profile</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="projects.html" title="">
                                <span><img src="{{ asset("social/images/icon3.png") }}" alt=""></span>
                                Projects
                            </a>
                        </li>

                        <li>
                            <a href="jobs.html" title="">
                                <span><img src="{{ asset("social/images/icon5.png") }}" alt=""></span>
                                Jobs
                            </a>
                        </li>
                        <li>
                            <a href="#" title="" class="not-box-openm">
                                <span><img src="{{ asset("social/images/icon6.png") }}" alt=""></span>
                                Messages
                            </a>
                            <div class="notification-box msg" id="message">
                                <div class="nt-title">
                                    <h4>Setting</h4>
                                    <a href="#" title="">Clear all</a>
                                </div>
                                <div class="nott-list">
                                    <div class="notfication-details">
                                        <div class="noty-user-img">
                                            <img src="{{ asset("social/images/resources/ny-img1.png") }}" alt="">
                                        </div>
                                        <div class="notification-info">
                                            <h3><a href="messages.html" title="">Jassica William</a> </h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do.</p>
                                            <span>2 min ago</span>
                                        </div><!--notification-info -->
                                    </div>
                                    <div class="notfication-details">
                                        <div class="noty-user-img">
                                            <img src="{{ asset("social/images/resources/ny-img2.png") }}" alt="">
                                        </div>
                                        <div class="notification-info">
                                            <h3><a href="messages.html" title="">Jassica William</a></h3>
                                            <p>Lorem ipsum dolor sit amet.</p>
                                            <span>2 min ago</span>
                                        </div><!--notification-info -->
                                    </div>
                                    <div class="notfication-details">
                                        <div class="noty-user-img">
                                            <img src="{{ asset("social/images/resources/ny-img3.png") }}" alt="">
                                        </div>
                                        <div class="notification-info">
                                            <h3><a href="messages.html" title="">Jassica William</a></h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempo incididunt ut labore et dolore magna aliqua.</p>
                                            <span>2 min ago</span>
                                        </div><!--notification-info -->
                                    </div>
                                    <div class="view-all-nots">
                                        <a href="messages.html" title="">View All Messsages</a>
                                    </div>
                                </div><!--nott-list end-->
                            </div><!--notification-box end-->
                        </li>
                        <li>
                            <a href="#" title="" class="not-box-open">
                                <span><img src="{{ asset("social/images/icon7.png") }}" alt=""></span>
                                Notification
                            </a>
                            <div class="notification-box noti" id="notification">
                                <div class="nt-title">
                                    <h4>Setting</h4>
                                    <a href="#" title="">Clear all</a>
                                </div>
                                <div class="nott-list">
                                    <div class="notfication-details">
                                        <div class="noty-user-img">
                                            <img src="social/images/resources/ny-img1.png" alt="">
                                        </div>
                                        <div class="notification-info">
                                            <h3><a href="#" title="">Jassica William</a> Comment on your project.</h3>
                                            <span>2 min ago</span>
                                        </div><!--notification-info -->
                                    </div>
                                    <div class="notfication-details">
                                        <div class="noty-user-img">
                                            <img src="social/images/resources/ny-img2.png" alt="">
                                        </div>
                                        <div class="notification-info">
                                            <h3><a href="#" title="">Jassica William</a> Comment on your project.</h3>
                                            <span>2 min ago</span>
                                        </div><!--notification-info -->
                                    </div>
                                    <div class="notfication-details">
                                        <div class="noty-user-img">
                                            <img src="social/images/resources/ny-img3.png" alt="">
                                        </div>
                                        <div class="notification-info">
                                            <h3><a href="#" title="">Jassica William</a> Comment on your project.</h3>
                                            <span>2 min ago</span>
                                        </div><!--notification-info -->
                                    </div>
                                    <div class="notfication-details">
                                        <div class="noty-user-img">
                                            <img src="social/images/resources/ny-img2.png" alt="">
                                        </div>
                                        <div class="notification-info">
                                            <h3><a href="#" title="">Jassica William</a> Comment on your project.</h3>
                                            <span>2 min ago</span>
                                        </div><!--notification-info -->
                                    </div>
                                    <div class="view-all-nots">
                                        <a href="#" title="">View All Notification</a>
                                    </div>
                                </div><!--nott-list end-->
                            </div><!--notification-box end-->
                        </li>
                    </ul>
                </nav><!--nav end-->
                <div class="menu-btn">
                    <a href="#" title=""><i class="fa fa-bars"></i></a>
                </div><!--menu-btn end-->
                <div class="user-account">
                    <div class="user-info">
                        <img src="{{ $avatar}} " height="31" width="31">
                        <a href="#" title="">{{ $nickname }}</a>
{{--                        <i class="la la-sort-down"></i>--}}
                    </div>
                    <div class="user-account-settingss" id="users">

                        <h3>Setting</h3>
                        <ul class="us-links">
                            <li><a href="profile-account-setting.html" title="">Account Setting</a></li>
                            <li><a href="#" title="">Privacy</a></li>
                            <li><a href="#" title="">Faqs</a></li>
                            <li><a href="#" title="">Terms & Conditions</a></li>
                        </ul>
                        <h3 class="tc">
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </h3>
                    </div><!--user-account-settingss end-->
                </div>
            </div><!--header-data end-->
        </div>
    </header><!--header end-->

    @yield("body")


</div><!--theme-layout end-->
@extends('layouts.footer')



<script type="text/javascript" src="{{ asset("social/js/jquery.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("social/js/popper.js") }}"></script>
<script type="text/javascript" src="{{ asset("social/js/bootstrap.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("social/js/jquery.mCustomScrollbar.js") }}"></script>
<script type="text/javascript" src="{{ asset("social/lib/slick/slick.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("social/js/scrollbar.js") }}"></script>
<script type="text/javascript" src="{{ asset("social/js/script.js") }}"></script>

</body>
</html>
