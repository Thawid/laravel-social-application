@extends("layouts.main")
@section("body")
    <main>
        <div class="main-section">
            <div class="container">
                <div class="main-section-data">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 pd-left-none no-pd">
                            <div class="main-left-sidebar no-margin">
                                <div class="user-data full-width">
                                    <div class="user-profile">
                                        <div class="username-dt">
                                            <div class="usr-pic">
                                                <img src="{{ $avatar }}" height="100" width="100">
                                            </div>
                                        </div><!--username-dt end-->
                                        <div class="user-specs">
                                            <h3> {{ $userName }}</h3>
                                            <span>Graphic Designer at Self Employed</span>
                                        </div>
                                    </div><!--user-profile end-->
                                    <ul class="user-fw-status">
                                        <li>
                                            <a href="{{ asset('home/friendlist/'.$nickname) }}">
                                                <h4>Friends</h4>
                                                <span>{{ $totalFriend }}</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ asset('home/followinglist/'.$nickname) }}">
                                            <h4>Following</h4>
                                            <span> {{ $following }}</span>
                                            </a>
                                        </li>
                                        <li>
                                            <h4>Followers</h4>
                                            <span>{{ $follower }}</span>
                                        </li>
                                        <li>
                                            <a href="{{ asset('home/'.$nickname) }}" title="">View Profile</a>
                                        </li>
                                    </ul>
                                </div><!--user-data end-->
                            </div><!--main-left-sidebar end-->
                        </div>
                        <div class="col-lg-6 col-md-8 no-pd">
                            <div class="main-ws-sec">
                                <div class="post-project">
                                    <h3>Post New Status</h3>
                                    <div class="post-project-fields">
                                        <form method="POST" action="{{route('status.save')}}">
                                            @csrf
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <textarea name="status" placeholder="Description"></textarea>
                                                </div>
                                                <div class="col-lg-12">
                                                    <ul>
                                                        <li><button class="active" type="submit" value="post">Post</button></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div><!--post-project end-->
                                <span style="border: 1px solid #e5e5e5;">&nbsp;</span>
                                <div class="posts-section">
                                    @foreach($status as $allStatus)
                                    <div class="post-bar">
                                        <div class="post_topbar">
                                            <div class="usy-dt">
                                                <img src="{{ empty($allStatus->user->avatar)? asset('social/images/resources/pf-icon4.png') : $allStatus->user->avatar }}" height="50" width="50">
                                                <div class="usy-name">
                                                    <h3>{{ $allStatus->user->name }}</h3>
                                                    <span><img src="social/images/clock.png" alt="">{{ date('H:i A, dS M Y', strtotime($allStatus->created_at)) }}</span>
                                                </div>
                                            </div>
                                            <div class="ed-opts">
                                                <a href="#" title="" class="ed-opts-open"><i class="la la-ellipsis-v"></i></a>
                                                <ul class="ed-options">
                                                    <li><a href="javascript:void(0);" title="">Edit Post</a></li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="job_descp">

                                            <p class="text-justify">{{ $allStatus['status'] }}...<a href="#" title="">view more</a></p>

                                        </div>
                                        <div class="job-status-bar">
                                            <ul class="like-com">
                                                <li>
                                                    <a href="#"><i class="fas fa-heart"></i> Like</a>
                                                    <img src="social/images/liked-img.png" alt="">
                                                    <span>25</span>
                                                </li>
                                                <li><a href="#" class="com"><i class="fas fa-comment-alt"></i> Comment 15</a></li>
                                            </ul>
                                            <a href="#"><i class="fas fa-eye"></i>Views 50</a>
                                        </div>
                                    </div><!--post-bar end-->
                                    @endforeach
                                     <!------------------------top profile -------------------------->
                                    {{--load more post section--}}
                                </div><!--posts-section end-->
                            </div><!--main-ws-sec end-->
                        </div>
                        <div class="col-lg-3 pd-right-none no-pd">
                            <div class="right-sidebar">
                                <div class="widget widget-about">
                                    <img src="social/images/wd-logo.png" alt="">
                                    <h3>Track Time on Workwise</h3>
                                    <span>Pay only for the Hours worked</span>
                                    <div class="sign_link">
                                        <h3>
                                            <a href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                                @csrf
                                            </form>
                                        </h3>

                                    </div>
                                </div><!--widget-about end-->
                                <div class="suggestions full-width">
                                    <div class="sd-title">
                                        <h3>Suggestions</h3>
                                        <i class="la la-ellipsis-v"></i>
                                    </div><!--sd-title end-->
                                    <div class="suggestions-list">
                                        <div class="suggestion-usd">
                                            <img src="social/images/resources/s1.png" alt="">
                                            <div class="sgt-text">
                                                <h4>Jessica William</h4>
                                                <span>Graphic Designer</span>
                                            </div>
                                            <span><i class="la la-plus"></i></span>
                                        </div>
                                        <div class="suggestion-usd">
                                            <img src="social/images/resources/s2.png" alt="">
                                            <div class="sgt-text">
                                                <h4>John Doe</h4>
                                                <span>PHP Developer</span>
                                            </div>
                                            <span><i class="la la-plus"></i></span>
                                        </div>
                                        <div class="suggestion-usd">
                                            <img src="social/images/resources/s3.png" alt="">
                                            <div class="sgt-text">
                                                <h4>Poonam</h4>
                                                <span>Wordpress Developer</span>
                                            </div>
                                            <span><i class="la la-plus"></i></span>
                                        </div>
                                        <div class="suggestion-usd">
                                            <img src="social/images/resources/s4.png" alt="">
                                            <div class="sgt-text">
                                                <h4>Bill Gates</h4>
                                                <span>C & C++ Developer</span>
                                            </div>
                                            <span><i class="la la-plus"></i></span>
                                        </div>
                                        <div class="suggestion-usd">
                                            <img src="social/images/resources/s5.png" alt="">
                                            <div class="sgt-text">
                                                <h4>Jessica William</h4>
                                                <span>Graphic Designer</span>
                                            </div>
                                            <span><i class="la la-plus"></i></span>
                                        </div>
                                        <div class="suggestion-usd">
                                            <img src="social/images/resources/s6.png" alt="">
                                            <div class="sgt-text">
                                                <h4>John Doe</h4>
                                                <span>PHP Developer</span>
                                            </div>
                                            <span><i class="la la-plus"></i></span>
                                        </div>
                                        <div class="view-more">
                                            <a href="#" title="">View More</a>
                                        </div>
                                    </div><!--suggestions-list end-->
                                </div><!--suggestions end-->
                                {{--Most Viewed People Section ------}}
                            </div><!--right-sidebar end-->
                        </div>
                    </div>
                </div><!-- main-section-data end-->
            </div>
        </div>
    </main>

    {{-----Job Post Section --}}

    <div class="chatbox-list">
        {{----------chat box section--------}}
    </div><!--chatbox-list end-->
@endsection
