@extends('layouts.main')
@section('body')
    <section class="companies-info">
        <div class="container">
            <div class="company-title">
                <h3 class="text-center">All Friends</h3>
            </div><!--company-title end-->
            <div class="companies-list">
                <div class="row">
                    @foreach($userList as $list)
                        <div class="col-lg-3 col-md-4 col-sm-6">
                            <div class="company_profile_info">
                                <div class="company-up-info">
                                    <img src="{{ empty($list->avatar) ? asset('social/images/resources/user-pro-img.png') : $list->avatar }}" height="91" width="91" alt="">
                                    <h3>{{ $list->name }}</h3>

                                    <h4>Web Developer</h4>
                                </div>
                                <a href="{{ asset('home/user/'.$list->nickname) }}" title="" class="view-more-pro">View Profile</a>
                            </div><!--company_profile_info end-->
                        </div>
                    @endforeach
                </div>
            </div><!--companies-list end-->
            <div class="process-comm">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div><!--process-comm end-->
        </div>
    </section><!--companies-info end-->
    @endsection
