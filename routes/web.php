<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/{nickname}',[HomeController::class,'myProfile'])->name('myProfile');
Route::get('/home/user/{nickname}',[HomeController::class,'userProfile'])->name('user.profile');
Route::get('/userlist',[HomeController::class,'userList'])->name('userlist');
Route::post('/saveStatus',[HomeController::class,'saveStatus'])->name('status.save');
Route::post('/saveProfile',[HomeController::class,'saveProfile'])->name('profile.save');
Route::get('/home/makeFriend/{friendId}',[HomeController::class, 'makeFriend'])->name('public.makeFriend');
Route::get('/home/unFriend/{friendId}',[HomeController::class, 'unFriend'])->name('public.unFriend');
Route::get('/home/makeFollow/{friendId}',[HomeController::class, 'makeFollow'])->name('make.follow');
Route::get('/home/unfollow/{friendId}',[HomeController::class, 'unfollow'])->name('make.unFollow');
Route::get('/home/friendlist/{nickname}',[HomeController::class,'friendList'])->name('friend.list');
Route::get('/home/followinglist/{nickname}',[HomeController::class,'followingList'])->name('following.list');

// for testing
Route::get('/showPath',[TestController::class,'showPath']);

Route::get('/auth/redirect/{provider}', [SocialController::class,'redirect']);
Route::get('/callback/{provider}', [SocialController::class,'callback']);



