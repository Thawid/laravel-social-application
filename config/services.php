<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'facebook' => [
        'client_id' => '690610498317493',
        'client_secret' => 'bebf6ad264cfa25b36bb18a01470ec7c',
        'redirect' => 'https://socail.winnerdevs.com/callback/facebook',
    ],

    'google' => [
        'client_id' => '863809019130-3lb41e1eulmk4sohu01ht34k6lt4luak.apps.googleusercontent.com',
        'client_secret' => 'fFs3reO2s4Ky88aLVzzSmVFA',
        'redirect' => 'https://socail.winnerdevs.com/callback/google',
    ],

];
