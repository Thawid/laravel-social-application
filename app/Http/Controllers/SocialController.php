<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{
    public function redirect($provider){
        return Socialite::driver($provider)->redirect();
    }

    public function callback($provider){
        $getInfo = Socialite::driver($provider)->user();
        $user = $this->createUser($getInfo,$provider);
        auth()->login($user);
        return redirect()->to('/home');
    }

    function  createUser($getInfo,$provider){

       $user = User::where('provider_id',$getInfo->id)->first();
        if(!$user){
            $user = User::create([
                'name'=>$getInfo->name,
                'email'=>$getInfo->email,
                'nickname'=>$getInfo->name,
                'provider'=>$provider,
                'provider_id'=>$getInfo->id
            ]);
        }

        return $user;

     /*   try {
            $user = Socialite::driver($provider)->user();
            $finduser = User::where('provider_id', $user->id)->first();
            if ($finduser) {
                Auth::login($finduser);
                return redirect('/home');
            } else {
                $newUser = User::create(['name' => $user->name, 'email' => $user->email, 'provider_id' => $user->id]);
                Auth::login($newUser);
                return redirect()->back();
            }
        }
        catch(Exception $e) {
            return redirect('/auth/redirect/{$provider}');
        }*/

    }
}
