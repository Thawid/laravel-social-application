<?php

namespace App\Http\Controllers;
use App\Follower;
use App\Friend;
use App\Status;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\RedirectResponse;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $userId = Auth::id();
        if(Friend::where('user_id', $userId)->where('friend_id',$userId)->count() == 0){
            $friendShip = new Friend();
            $friendShip->user_id = $userId;
            $friendShip->friend_id = $userId;
            $friendShip->save();
        }
        $userName = Auth::user()->name;
        $totalFriend = $this->numberOfFriend();
        $following = $this->numberOfFollowing();
        $follower = $this->numberOfFollower();
        $avatar = $this->avatar();
        $nickname = $this->nickName();
        //$status = Status::where('user_id',$userId)->orderBy('id','DESC')->get();
        $status = Auth::user()->friendShip;
        return view("home", [
            'status'=>$status,
            'avatar' => $avatar,
            'userName' =>$userName,
            'nickname'=>$nickname,
            'totalFriend'=>$totalFriend,
            'following'=>$following,
            'follower'=>$follower,
        ]);
    }

    protected function numberOfFriend(){
        $userId = Auth::id();
        $totalFriend = Friend::where('user_id',$userId)->count()-1;
        return $totalFriend;
    }

    protected function numberOfFollowing(){
        $userId = Auth::id();
        $following = Follower::where('user_id',$userId)->count();
        return $following;
    }

    protected function numberOfFollower(){
        $userId = Auth::id();
        $follower = Follower::where('friend_id',$userId)->count();
        return $follower;
    }

    public function saveStatus(Request $request){
        if(Auth::check()){
            $status = $request->post('status');
            $userId = Auth::id();
            $statusModel = new Status();
            $statusModel->status = $status;
            $statusModel->user_id = $userId;
            $statusModel->save();
            return redirect()->route('home');
        }
    }

    public function myProfile($nickName){

        if(Auth::check()){
            $user = User::where('nickname',$nickName)->first();;
            $following = $this->numberOfFollowing();
            $follower = $this->numberOfFollower();
            $name = Auth::user()->name;
            $nickname = $this->nickName();
            $avatar = $this->avatar();
            $profilView  = Status::where('user_id', $user->id)->orderBy('id', 'desc')->get();
            return view('myProfile',[
                'profile'=>$profilView,
                'avatar' => $avatar,
                'name'=>$name,
                'nickname'=>$nickname,
                'following'=>$following,
                'follower'=>$follower,

            ]);
        }
    }

    public function saveProfile(Request $request){
        if(Auth::check()){
            $user = Auth::user();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->nickname = $request->nickname;
            if($request->avatar){
                $profileImage = 'user'.$user->id.'.'.$request->file('avatar')->extension();
                $request->avatar->move(public_path('social/profile_pic'),$profileImage);
                $user->avatar = asset("social/profile_pic/{$profileImage}");
            }
            $user->save();
            return redirect()->route('myProfile');
        }

    }

    public function userProfile($nickname) {
        $user = User::where('nickname',$nickname)->first();
        if($user){
            $name = $user->name;
            $checkFriend = $this->checkFriend($nickname);
            $checkFollowing = $this->checkFollowing($nickname);
            $avatar = $this->avatar();
            $profileAvatar = $this->profileAvatar($nickname);
            $nickname = $this->nickName();
            $status = Status::where('user_id',$user->id)->orderBy('id','DESC')->get();
            $displayAction = false;
            if(Auth::check()){
                if(Auth::user()->id != $user->id){
                    $displayAction = true;
                }
            }
            return view("userProfile", [
                'status'=>$status,
                'profileAvatar' => $profileAvatar,
                'avatar' => $avatar,
                'name' =>$name,
                'displayAction'=>$displayAction,
                'friendId' => $user->id,
                'nickname' =>$nickname,
                'checkfriend'=>$checkFriend,
                'checkFollowing'=>$checkFollowing
            ]);
        }else{
            return redirect('home');
        }
    }

    public function userList() {
        $user = Auth::user();
        $avatar = $this->avatar();
        $nickname = $this->nickName();
        $userList = DB::table('users')
            ->where('id','!=',$user->id)
            ->get();
        return view('userList', [
            'userList' => $userList,
            'avatar' => $avatar,
            'nickname' => $nickname,
        ]);

    }

    public function makeFriend($friendId){
        $friendShip = new Friend();
        $userId = Auth::user()->id;
        If(Friend::where('user_id',$userId)->where('friend_id', $friendId)->count() == 0){
            $friendShip->user_id = $userId;
            $friendShip->friend_id = $friendId;
            $friendShip->save();
        }
        if (Friend::where('friend_id', $userId)->where('user_id', $friendId)->count() == 0) {
            $friendShip = new Friend();
            $friendShip->friend_id = Auth::user()->id;
            $friendShip->user_id = $friendId;
            $friendShip->save();
        }
        return redirect()->route('home');
    }

    public  function unFriend($friendId){
        $userId = Auth::user()->id;
        Friend::where('user_id',$userId)->where('friend_id',$friendId)->delete();
        Friend::where('friend_id',$userId)->where('user_id',$friendId)->delete();
        return redirect()->route('home');
    }

    public function makeFollow($friendId){
        //$nickname = User::select('nickname')->where('id',$friendId)->first();

        $follower = new Follower();
        $userId = Auth::user()->id;
        if(Follower::where('user_id',$userId)->where('friend_id',$friendId)->count() == 0){
            $follower->user_id = $userId;
            $follower->friend_id = $friendId;
            $follower->save();
        }
        //return redirect()->route('home');
        return redirect()->back();

    }

    public function unfollow($friendId){
        //$nickname = User::select('nickname')->where('id',$friendId)->first();
        $userId = Auth::user()->id;
        Follower::where('user_id',$userId)->where('friend_id',$friendId)->delete();
        //return redirect()->route('home');
        return redirect()->back();
    }

    public function friendList($nickname){
        $user = Auth::user();
        $avatar = $this->avatar();
        $nickname = $this->nickName();
        $selectFriend = DB::table('users')
            ->leftJoin('friends','users.id','=','friends.friend_id')
            ->where([
                'friends.user_id'=>$user->id,
            ])->where('friends.friend_id','!=',$user->id)->get();
        //dd($selectFriend);
        return view('friendList', [
            'friendList' => $selectFriend,
            'avatar'=>$avatar,
            'nickname'=>$nickname,
        ]);
    }

    public function followingList($nickname){
        $user = Auth::user();
        $avatar = $this->avatar();
        $nickname = $this->nickName();
        $selectFollowing = DB::table('users')
            ->leftJoin('followers','users.id','=','followers.friend_id')
            ->where([
                'followers.user_id'=>$user->id,
            ])->get();
        //dd($selectFollowing);
        return view('followingList', [
            'followingList' => $selectFollowing,
            'avatar'=>$avatar,
            'nickname'=>$nickname,
        ]);
    }

    protected function checkFriend($nickname){
        //$user = Auth::user();
        $user = User::where('nickname',$nickname)->first();
        $checkfriend = false;
        if(Friend::where('user_id',Auth::user()->id)->where('friend_id',$user->id)->count() == 1) {
            $checkfriend = true;
        }
        return $checkfriend;
    }

    protected function checkFollowing($nickname){
        $user = User::where('nickname',$nickname)->first();
        $checkFollowing = false;
        if(Follower::where('user_id',Auth::user()->id)->where('friend_id',$user->id)->count() == 1){
            $checkFollowing = true;
        }
        return $checkFollowing;
    }

    protected  function nickName(){
        $nickname = empty(Auth::user()->nickname) ? "User" : Auth::user()->nickname;
        return $nickname;
    }

    protected  function avatar(){
        $avatar = empty(Auth::user()->avatar) ? asset('social/images/resources/user-pro-img.png') : Auth::user()->avatar;
        return $avatar;
    }

    protected function profileAvatar($nickname){
        $user = User::where('nickname',$nickname)->first();
        $profileAvatar = empty($user->avatar) ? asset('social/images/resources/user-pro-img.png') : $user->avatar;
        return $profileAvatar;
    }

}
